#!/bin/bash


if [ ! -e /tmp/ttyVUSB0 ]; then
	echo "Openning ports ttyVUSB0 connected to ttyVUSB1"
	socat -d -d -d -d PTY,mode=0777,rawer,echo=0,link=/tmp/ttyVUSB0  PTY,mode=0777,raw,echo=0,link=/tmp/ttyVUSB1 >& /dev/null &
fi
if [ ! -e /tmp/ttyVUSB2 ]; then
	echo "Openning ports ttyVUSB2 connected to ttyVUSB3"
	socat -d -d -d -d PTY,mode=0777,rawer,echo=0,link=/tmp/ttyVUSB2  PTY,mode=0777,raw,echo=0,link=/tmp/ttyVUSB3 >& /dev/null &
fi

echo "Port ready"
