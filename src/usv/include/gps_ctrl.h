#ifndef GPS_CTRL_H
#define GPS_CTRL_H

#include "config.h"

#include <iostream>
#include <math.h>

#include <sensor_msgs/NavSatFix.h>

#include <ros/ros.h>

#include <QObject>
#include <QTimer>
#include <QDebug>
#include <QSettings>
#include <QGeoCoordinate>
#include <QGeoPositionInfo>

using namespace std;

class Gpsctrl : public QObject
{
    Q_OBJECT

    public :
        Gpsctrl();
        ~Gpsctrl();

        double head_err;    // Acceptable error on heading (in deg) to set FW speed
        double dist_stop;   // Stopping distance to arrival wp (in m)
        double dist_slow;   // Slow speed distance to arrival wp (in m)
        unsigned int power_speed_fast;  // Power speed between two wp (in %)
        unsigned int power_speed_slow;  // Power speed at dist slow to wp (in %)

        //// Inputs ////
        QGeoCoordinate pos_des; // Desired and measured pos
        QGeoCoordinate pos_mes;
        
        //// Outputs ////
        double head_out;        // Heading to goal (in deg)
        double dist;            // Distance to goal (in m)

    public slots :
        // Default parameters
        void set_def_param();

        // Load/store settings
        void read_settings();
        void write_settings();

        // Set/get
        void set_pos_des(sensor_msgs::NavSatFix pos);
        void set_pos_mes(QGeoPositionInfo pos);
        sensor_msgs::NavSatFix get_pos_des();
        sensor_msgs::NavSatFix get_pos_mes();

        // Others
        void update();

    private :
};

#endif
