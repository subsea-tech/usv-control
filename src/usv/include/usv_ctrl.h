#ifndef USV_CTRL_H
#define USV_CTRL_H

#include "config.h"
#include "model.h"
#include "controller.h"
#include "joy_ctrl.h"
#include "gps_ctrl.h"

#include "marnav/io/default_nmea_reader.hpp"
#include "marnav/io/default_nmea_serial.hpp"
#include "marnav/nmea/nmea.hpp"
#include "marnav/nmea/io.hpp"
#include "marnav/utils/unique.hpp"
#include "marnav/nmea/constants.hpp"
#include "marnav/nmea/rmb.hpp"
#include "marnav/nmea/rmc.hpp"
#include "marnav/nmea/hdt.hpp"
#include "marnav/nmea/vdm.hpp"
#include "marnav/nmea/rte.hpp"

#include <ros/ros.h>
#include <std_msgs/UInt16.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float32MultiArray.h>
#include <geometry_msgs/Point.h>
#include <nmea_msgs/Sentence.h>
#include <sensor_msgs/Joy.h>
#include <nav_msgs/Path.h>
#include <msg_usv/nav_ctrl_cmd.h>
#include <msg_usv/ctrl_param.h>
#include <msg_usv/ctrl_values.h>
#include <msg_usv/esc_cmd.h>

#include <iostream>
#include <csignal>

#include <QCoreApplication>
#include <QTimer>
#include <QGeoCoordinate>
#include <QGeoPositionInfo>
#include <QBuffer>
#include <QDebug>
#include <QTranslator>
#include <QThread>

using namespace marnav;

using namespace msg_usv;

struct Gps
{
    QGeoPositionInfo pos_info;
    double sog; // in knots
    double cog;
    bool valid;
};

class Usv_ctrl : public QCoreApplication
{
    Q_OBJECT

	public:
        explicit Usv_ctrl(int argc, char **argv, ros::NodeHandle * nh);

    private :
        //ROS
		ros::NodeHandle* _Nh;
        ros::Publisher _esc_right_pub, _esc_left_pub, _ctrl_val_pub, _ctrl_param_pub, _nav_ctrl_pub;
        ros::Subscriber _nmea0183_sentence_sub, _joy_sub, _nav_ctrl_sub, _new_waypoints_mission_sub,
                        _ctrl_param_sub, _enable_sub;

        // Timers
        QTimer _timer_ros;
        QTimer _timer_ctrl;
        QTimer _gps_wd;         // Watch dog GPS
        QTimer _compass_wd;     // Watch dog compass
        QTimer _nav_frame_wd;   // Watch dog navigation frame (RMB)

        // Vars
        bool _enable = false;
        Gps _gps;
        double _heading_true = 0;                // in deg
        msg_usv::nav_ctrl_cmd _nav_ctrl_cmd;
        sensor_msgs::NavSatFix _autopilot_position_goto_order;

        // Outputs
        msg_usv::esc_cmd _esc_left_cmd;
        msg_usv::esc_cmd _esc_right_cmd;

        // Parameters and Controllers
        Parameters _p;
        Joyctrl _joyctrl;
        Autohead _autohead;
        Autospeed _autospeed;
        Model _model;
        Gpsctrl _gpsctrl;

    private slots :
        // Methods
        void stop_ctrlers();
        void enable_esc(bool on);

	public :
        // Callbacks
        void nmea0183_sentence_callback(nmea_msgs::Sentence msg);
        void joy_callback(sensor_msgs::Joy msg);
        void nav_ctrl_cmd_callback(msg_usv::nav_ctrl_cmd msg);
        void ctrl_param_callback(const ros::MessageEvent<msg_usv::ctrl_param const>& event);
        void enable_callback(const std_msgs::Bool msg);

    public slots :
        // Methods to publish
        void publish_esc_cmd();
        void publish_ctrl_values();
        void publish_ctrl_param();  // Used to inform GUI of actual parameters
        void publish_ctrl_mode(uint16_t mode);

        // Others
        void update_ctrl();
        void update_ctrl_mode(uint16_t mode);

    signals :

    //protected:
};

#endif
