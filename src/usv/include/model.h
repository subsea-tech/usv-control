#ifndef MODEL_H
#define MODEL_H

#include "config.h"

#include <iostream>
#include <math.h>

#include <QObject>
#include <QTimer>
#include <QDebug>

class Parameters
{
    public :
        Parameters();
        ~Parameters();

        double m;           // mass in kg
        double Iz;          // Yaw inertia in kg.m²
        double cT;          // Thrust coeff in N/(RPM²)
        int rpm_max;        // in RPM
        double l;           // distance between propellers in m
        int dt_ctrl_ms;     // Ctrl period in ms

        // Get max forces and torque
        double get_fx_max();
        double get_tz_max();
};

class Model : public QObject
{
    Q_OBJECT

    public :
        Model(Parameters * p);
        ~Model();

        // Compute RPM with fx and tz order (in N and N.m)
        void compute_rpm_order();

        // Outputs
        int16_t rpm_left;
        int16_t rpm_right;

        // Inputs
        double fx; // Force FW (in N)
        double tz; // Torque Z (in N.m)

    private :
        Parameters * _param;
};

#endif
