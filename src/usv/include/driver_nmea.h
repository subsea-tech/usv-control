#ifndef DRIVER_NMEA_H
#define DRIVER_NMEA_H

#include "config.h"

#include "marnav/io/default_nmea_reader.hpp"
#include "marnav/io/default_nmea_serial.hpp"
#include "marnav/io/nmea_reader.hpp"
#include "marnav/nmea/nmea.hpp"
#include "marnav/nmea/sentence.hpp"
#include "marnav/nmea/constants.hpp"
#include "marnav/nmea/io.hpp"
#include "marnav/nmea/talker_id.hpp"
#include "marnav/utils/unique.hpp"
#include "marnav/nmea/rmc.hpp"
#include "marnav/nmea/hdt.hpp"
#include "marnav/nmea/hdg.hpp"

#include <ros/ros.h>
#include <std_msgs/UInt16.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float32MultiArray.h>
#include <geometry_msgs/Point.h>
#include <sensor_msgs/NavSatFix.h>
#include <nmea_msgs/Sentence.h>

#include <iostream>
#include <csignal>

#include <QCoreApplication>
#include <QTimer>
#include <QGeoCoordinate>
#include <QGeoPositionInfo>
#include <QBuffer>
#include <QDebug>
#include <QThread>
#include <QMetaType>

//#define DEBUG_NMEA

#define DEFAULT_NMEA_BAUD 4800
#define DEFAULT_AIS_BAUD 38400

#define CUSTOM_TALKER_ID "II"   // Should be known by marnav lib (see talker_id.cpp)

Q_DECLARE_METATYPE(nmea_msgs::Sentence)

using namespace marnav;
using namespace marnav::io;

class Nmea_reader_thread : public QThread
{
    Q_OBJECT

    void run() override;

    public :
        Nmea_reader_thread(std::string name);
        void init(const std::string & dev_name, const int & baud);

    private :
        std::string _thread_name;   // To identify thread
        std::string _dev_name;
        serial::baud _baud; // Baudrate (default 4800)

    signals :
        void sentence_received(QVariant);
};

class Driver_nmea : public QCoreApplication
{
    Q_OBJECT

	public:
        explicit Driver_nmea(int argc, char **argv, ros::NodeHandle * nh);
		~Driver_nmea();
        void init_vars();

    private :
        //ROS
		ros::NodeHandle* _Nh;
        ros::Publisher _nmea0183_sentence_pub, _gps_pub, _sog_pub, _proj_sog_pub, _cog_pub, _hdt_pub, _hdg_pub;

        // Vars
        Nmea_reader_thread _gps_thread, _compass_thread, _ais_thread;
        double _mag_dev;    // Magnetic deviation (in signed deg)
        float _sog = 0, _cog = 0, _hdt = 0;

        // Timers
        QTimer _timer_ros;

        // Others
        void parse_nmea(nmea_msgs::Sentence msg);
        void send_HDT_frame(double h_t);
        void publish_proj_sog();

    public :

    public slots :
        // Timer callbacks
        void timer_ros_callback();

        // Methods to publish
        void publish_nmea0183_sentence(QVariant);

        // Others
};

#endif
