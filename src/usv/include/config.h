#ifndef CONFIG_H
#define CONFIG_H

// ROS Spin period (used for all nodes)
#define TIME_MS_ROS_SPIN 10

// Settings folder (in .config/)
#define SETTINGS_FOLDER "USV Package"

// Seacat Control default parameters
#define CTRL_M          1600        // in kg
#define CTRL_IZ         6600        // in kg.m²
#define CTRL_CT         1.024e-4    // in N/RPM²
#define CTRL_RPM_MAX    2500        // in RPM
#define CTRL_L          1.6         // in m
#define CTRL_DT_CTRL    10          // in ms

// Default values for controllers
#define KP_Z        0.005
#define KI_Z        0.002
#define KD_Z        0.
#define I_SAT_IN_Z  100
#define I_SAT_OUT_Z 1000.
#define I_THRESH_Z  4.
#define KFF_V       0.
#define KP_V        0.01
#define KI_V        0.005
#define I_SAT_V     2000.

// Default values for speed during GPS/OpenCPN goto
#define HEAD_ERR_THRESH 20      // In deg
#define POWER_SPEED_FAST 80     // In % of max power
#define POWER_SPEED_SLOW 15     // Slow speed when approaching wp
#define FAST_SPEED 3.5          // In knots
#define SLOW_SPEED 1            // In knots
#define THRESH_DIST_SLOW 30     // Distance from waypoint to pass in slow speed (in m)
#define THRESH_DIST_STOP 5      // Distance from waypoint to stop (in m) only used for GPS ctrl

// Time Out for ctrl when nothing rcv from NMEA sensor
#define TIME_OUT_MS_NMEA 5000

// Useful define
#define PI 3.14159265
#define D2R (PI/180.0)
#define R2D (180.0/PI)
#define M2NM (1/1.852/1000.0)
#define NM2M (1/M2NM)
#define KT2MS (1.852/3.6)
#define MS2KT (1/KT2MS)

template <typename T> int sign(T val) {
    return (T(0) < val) - (val < T(0));
}

#endif
