#ifndef JOY_CTRL_H
#define JOY_CTRL_H

#include "config.h"
#include "model.h"

#include <iostream>
#include <math.h>

#include <QObject>
#include <QTimer>
#include <QDebug>
#include <QSettings>

using namespace std;

namespace JoyMode
{
    typedef enum
    {
        MODE1
    }EJoyMode;
}

class Joyctrl : public QObject
{
    Q_OBJECT

    public :
        Joyctrl(Parameters * p);
        ~Joyctrl();

        int k1;                 // FW smoothness [-100% : aggressive ; 100% : smooth]
        int k2;                 // LR smoothness (in %)
        JoyMode::EJoyMode mode;

        //// Inputs ////
        double joy_fw;          // [-1 1] > 0 FW
        double joy_lr;          // > 0 Right
        
        //// Outputs ////
        double f_out;           // Force FW (in N) > 0 <=> FW
        double t_out;           // Torque Left/Right (in N.m) > 0 <=> Go left

    public slots :
        void set_def_param();   // Default parameters
        void read_settings();   // Load/store settings
        void write_settings();

        void update();

    private :
        Parameters * _param;
};

#endif
