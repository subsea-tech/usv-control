#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "config.h"
#include "model.h"

#include <iostream>
#include <csignal>
#include <deque>
#include <vector>
#include <math.h>

#include <QObject>
#include <QTimer>
#include <QGeoCoordinate>
#include <QGeoPositionInfo>
#include <QBuffer>
#include <QDebug>
#include <QSettings>

using namespace std;

class Autohead : public QObject
{
    Q_OBJECT

	public :
        Autohead(Parameters * p);
        ~Autohead();
        void init(); // To init filters and ctrl_period member

        //// Parameters ////
        bool active;
        double kp;
        double ki;
        double i_sat_in;            // Saturation integrator input / output of integrator
        double i_sat_out;
        double i_omega_thresh;      // Threshold on omega to hold the integrator (in deg/s)
        double kd;

        //// Inputs ////
        double psi_des;             // in rad [-inf inf] Different from heading !
        double psi_mes;

        //// Outputs ////
        double t_out;               // Torque Left/Right (in N.m) > 0 <=> Go left

        //// Ctrl Values ////
        double err;                 // Ctrl Error
        double dpsi_fil;            // Estimated omega
        double p_val;
        double i_val;
        double d_val;

    public slots :
        void set_def_param();       // Set all parameters to default values
        void read_settings();       // Load/store settings
        void write_settings();

        void start();               // Start/stop control
        void stop();
        void reset_int();           // Reset integrator

        void set_head_des(double head); // heading in [0 359.9] deg
        void set_head_mes(double head);

        float get_err_deg(); // Get error in [-180 ; 180] deg

        bool isActive() { return active; };

        void update();              // update ctrl

    signals :
        void started();

    private :
        Parameters * _param;
        double _ctrl_period;        // in s
        double _integrator;         // Integrated error

        //// Psi Filter vars ////
        unsigned int _n_psi_fil;    // Window size (odd number)
        double _fc_psi_fil;         // Cut off freq (in Hz)
        vector<double> _h_psi_fil;  // FIR Values
        deque<double> _dpsi_mes_q;  // List of past values (used for filtering)

        //// Dot omega estimation ////
        unsigned int _n_fil_dot;    // Odd number used to compute median
        deque<double> _derr_q;      // List of past values (used for median filtering)

        double compute_dot();       // Compute derivate and filter it
        double dpsi_filter();       // Filter dpsi value
        void synthetize_dpsi_fir(); // Synthetize psi fir (with n, fc values)
};

class Autospeed : public QObject
{
    Q_OBJECT

    public :
        Autospeed(Parameters * p);
        ~Autospeed();
        void init();            // To init filters and ctrl_period member

        //// Parameters ////
        bool active;
        double kff;             // Feed fw
        double kp;
        double ki;
        double i_sat;           // Saturation integrator (in N)

        //// Inputs ////
        double v_des;           // in m/s
        double v_mes;

        //// Outputs ////
        double f_out;           // Force FW (in N) > 0 <=> FW

        //// PID Values ////
        double err;             // Ctrl Error
        double ff_val;
        double p_val;
        double i_val;

    public slots :
        void set_def_param();   // Set default parameters
        void read_settings();   // Load/store settings
        void write_settings();

        void start();           // Start/stop control
        void stop();
        void reset_int();       // Reset integrator

        void set_v_des(double v_kt);// in knots
        void set_v_mes(double v_kt);

        bool isActive() { return active; };

        void update();          // update ctrl

    signals :
        void started();

    private :
        Parameters * _param;
        double _ctrl_period;    // in s
        double _integrator;     // Integrated error
};

#endif
