#include "gps_ctrl.h"

Gpsctrl::Gpsctrl() : QObject()
{
    this->set_def_param();

    this->read_settings();
}

Gpsctrl::~Gpsctrl()
{

}

void Gpsctrl::set_def_param()
{
    head_err = HEAD_ERR_THRESH;
    dist_stop = THRESH_DIST_STOP;
    dist_slow = THRESH_DIST_SLOW;
    power_speed_fast = POWER_SPEED_FAST;
    power_speed_slow = POWER_SPEED_SLOW;
}

void Gpsctrl::read_settings()
{
    QSettings settings(SETTINGS_FOLDER, "gps_ctrler");

    head_err = settings.value("head_err_thresh",head_err).toDouble();
    dist_stop = settings.value("dist_stop",dist_stop).toDouble();
    dist_slow = settings.value("dist_slow",dist_slow).toDouble();
    power_speed_fast = settings.value("power_speed_fast",power_speed_fast).toUInt();
    power_speed_slow = settings.value("power_speed_slow",power_speed_slow).toUInt();

    this->write_settings(); // Write Settings if keys not already exist
}

void Gpsctrl::write_settings()
{
    QSettings settings(SETTINGS_FOLDER, "gps_ctrler");

    settings.setValue("head_err_thresh",head_err);
    settings.setValue("dist_stop",dist_stop);
    settings.setValue("dist_slow",dist_slow);
    settings.setValue("power_speed_fast",power_speed_fast);
    settings.setValue("power_speed_slow",power_speed_slow);
}

void Gpsctrl::set_pos_des(sensor_msgs::NavSatFix pos)
{
    pos_des = QGeoCoordinate(pos.latitude,pos.longitude);
}

void Gpsctrl::set_pos_mes(QGeoPositionInfo pos)
{
    pos_mes = pos.coordinate();
}

sensor_msgs::NavSatFix Gpsctrl::get_pos_des()
{
    sensor_msgs::NavSatFix pos;
    pos.latitude = pos_des.latitude();
    pos.longitude = pos_des.longitude();

    return pos;
}

sensor_msgs::NavSatFix Gpsctrl::get_pos_mes()
{
    sensor_msgs::NavSatFix pos;
    pos.latitude = pos_mes.latitude();
    pos.longitude = pos_mes.longitude();

    return pos;
}


void Gpsctrl::update()
{
    head_out = pos_mes.azimuthTo(pos_des);
    dist = pos_mes.distanceTo(pos_des);
}
