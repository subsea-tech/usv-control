#include "joy_ctrl.h"

Joyctrl::Joyctrl(Parameters * p) : QObject()
{
    joy_fw = 0;
    joy_lr = 0;

    this->set_def_param();

    _param = p;
    
    f_out = 0;
    t_out = 0;

    this->read_settings();
}

Joyctrl::~Joyctrl()
{

}

void Joyctrl::set_def_param()
{
    mode = JoyMode::MODE1;

    k1 = 0;
    k2 = 0;
}

void Joyctrl::read_settings()
{
    QSettings settings(SETTINGS_FOLDER, "joy_ctrler");

    k1 = settings.value("k1",k1).toInt();
    k2 = settings.value("k2",k2).toInt();
    mode = static_cast<JoyMode::EJoyMode>(settings.value("mode",static_cast<int>(mode)).toInt());

    this->write_settings(); // Write Settings if keys not already exist
}

void Joyctrl::write_settings()
{
    QSettings settings(SETTINGS_FOLDER, "joy_ctrler");

    settings.setValue("k1",k1);
    settings.setValue("k2",k2);
    settings.setValue("mode",static_cast<int>(mode));
}

void Joyctrl::update()
{
    switch (mode)
    {
        case JoyMode::MODE1 :
        {
            double f_max = _param->get_fx_max();
            double t_max = _param->get_tz_max();

            f_out = (k1/100.0*joy_fw*abs(joy_fw)+(1-k1/100.0)*joy_fw) * f_max;
            t_out = -(k2/100.0*joy_lr*abs(joy_lr)+(1-k2/100.0)*joy_lr) * t_max;

            break;
        }
    }
}
