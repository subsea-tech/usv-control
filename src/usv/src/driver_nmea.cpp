#include "driver_nmea.h"

static Driver_nmea *driver_nmea;

void sig_handler [[ noreturn ]] (int s)
{
    std::cout << "driver_nmea : Caught signal " << s << std::endl;

    driver_nmea->quit();

	exit(s);
}

int main(int argc, char *argv[])
{
	ros::init(argc, argv, "driver_nmea", ros::init_options::NoSigintHandler);
    ros::NodeHandle nh("~");

	signal(SIGINT,sig_handler);
	signal(SIGTERM,sig_handler);

    driver_nmea = new Driver_nmea(argc,argv,&nh);

    return driver_nmea->exec();
}


Driver_nmea::Driver_nmea(int argc, char **argv, ros::NodeHandle * nh) : QCoreApplication(argc,argv), _gps_thread("Gps"), _compass_thread("Gyrocompass"), _ais_thread("Ais")
{
    // Init members
	_Nh = nh;
    this->init_vars();

    // Very important : chg locale to use marnav library
    std::setlocale(LC_ALL, "C");

    // Configuration parameters
    std::string gps_dev, compass_dev, ais_dev;
    int gps_baud, compass_baud, ais_baud;
    std::string param_name("gps_dev");
    if (_Nh->hasParam(param_name))
        _Nh->getParam(param_name,gps_dev);
    param_name = "gps_baud";
    if (!_Nh->hasParam(param_name)) // Create param if doesn't exist
        _Nh->setParam(param_name,DEFAULT_NMEA_BAUD);
    _Nh->getParam(param_name,gps_baud);
    param_name = "compass_dev";
    if (_Nh->hasParam(param_name))
        _Nh->getParam(param_name,compass_dev);
    param_name = "compass_baud";
    if (!_Nh->hasParam(param_name)) // Create param if doesn't exist
        _Nh->setParam(param_name,DEFAULT_NMEA_BAUD);
    _Nh->getParam(param_name,compass_baud);
    param_name = "ais_dev";
    if (_Nh->hasParam(param_name))
        _Nh->getParam(param_name,ais_dev);
    param_name = "ais_baud";
    if (!_Nh->hasParam(param_name)) // Create param if doesn't exist
        _Nh->setParam(param_name,DEFAULT_AIS_BAUD);
    _Nh->getParam(param_name,ais_baud);

    // Publishers
    _nmea0183_sentence_pub = _Nh->advertise<nmea_msgs::Sentence>("/nmea0183_sentence", 100);
    _gps_pub = _Nh->advertise<sensor_msgs::NavSatFix>("/gps_pos", 10);
    _sog_pub = _Nh->advertise<std_msgs::Float32>("/sog", 10);
    _proj_sog_pub = _Nh->advertise<std_msgs::Float32>("/proj_sog", 10);
    _cog_pub = _Nh->advertise<std_msgs::Float32>("/cog", 10);
    _hdt_pub = _Nh->advertise<std_msgs::Float32>("/heading_true", 10);
    _hdg_pub = _Nh->advertise<std_msgs::Float32>("/heading_mag", 10);

    // Timers
    connect(&_timer_ros, SIGNAL(timeout()), this, SLOT(timer_ros_callback()));

    // GPS NMEA reader thread
    if (!gps_dev.empty())
    {
        ROS_INFO("Driver NMEA : Start GPS Acquisition Thread");
        connect(&_gps_thread, SIGNAL(sentence_received(QVariant)), this, SLOT(publish_nmea0183_sentence(QVariant)));
        _gps_thread.init(gps_dev,gps_baud);
        _gps_thread.start();
    }
    else
      ROS_ERROR_STREAM("Driver NMEA : Port COM error on %s" << gps_dev);

    // Compass NMEA reader thread
    if (!compass_dev.empty())
    {
        ROS_INFO("Driver NMEA : Start Compass Acquisition Thread");
        connect(&_compass_thread, SIGNAL(sentence_received(QVariant)), this, SLOT(publish_nmea0183_sentence(QVariant)));
        _compass_thread.init(compass_dev,compass_baud);
        _compass_thread.start();
    }

    // AIS NMEA reader thread
    if (!ais_dev.empty())
    {
        ROS_INFO("Driver NMEA : Start AIS Acquisition Thread");
        connect(&_ais_thread, SIGNAL(sentence_received(QVariant)), this, SLOT(publish_nmea0183_sentence(QVariant)));
        _ais_thread.init(ais_dev,ais_baud);
        _ais_thread.start();
    }
}

Driver_nmea::~Driver_nmea()
{
}

void Driver_nmea::init_vars()
{
    _timer_ros.start(TIME_MS_ROS_SPIN);

    _mag_dev = 0;
}

void Driver_nmea::timer_ros_callback()
{
    ros::spinOnce();
}

void Driver_nmea::parse_nmea(nmea_msgs::Sentence msg)
{
    static bool hdt_dev_exist = false; // True if HDT already received
    static bool gps_dev_exist = false; // True if RMC already received and valid

    std::string nmea_str = msg.sentence;

    try
    {
        // NMEA parse
        auto sentence = nmea::make_sentence(nmea_str);

        switch (sentence->id())
        {
            case nmea::sentence_id::RMC : // RMC sentence (GPS)
            {
                auto rmc = nmea::sentence_cast<nmea::rmc>(sentence.get());
                auto status = rmc->get_status();
                auto lat = rmc->get_lat();
                auto lon = rmc->get_lon();
                auto head_gps = rmc->get_heading();
                auto sog = rmc->get_sog().value();
                auto mag_dev = rmc->get_mag();
                auto mag_dev_hem = rmc->get_mag_hem();

                if (mag_dev_hem.value() == nmea::direction::east)
                    _mag_dev = static_cast<double>(mag_dev.value());
                else
                    _mag_dev = -static_cast<double>(mag_dev.value());

                // Publish GPS ROS msg
                sensor_msgs::NavSatFix gps_pos;
                gps_pos.header.stamp = ros::Time::now();
                gps_pos.header.frame_id = to_string(sentence->get_talker());
                if (status.value() == 'A')
                {
                    gps_dev_exist = true;
                    gps_pos.status.status = gps_pos.status.STATUS_FIX;
                }
                else
                    gps_pos.status.status = gps_pos.status.STATUS_NO_FIX;
                gps_pos.latitude = lat->get();
                gps_pos.longitude = lon->get();
                gps_pos.altitude = 0;
                gps_pos.position_covariance_type = gps_pos.COVARIANCE_TYPE_UNKNOWN;
                _gps_pub.publish(gps_pos);

                // Publish SoG and CoG
                std_msgs::Float32 msg;
                _sog = sog.get<units::knots>().value();
                _cog = head_gps.value();
                msg.data = _sog; _sog_pub.publish(msg);
                msg.data = _cog; _cog_pub.publish(msg);

                // Update and publish projected sog
                this->publish_proj_sog();

                break;
            }

            case nmea::sentence_id::HDT : // HDT sentence (heading true, need GPS and compass)
            {
                auto hdt = nmea::sentence_cast<nmea::hdt>(sentence.get());
                auto head_true = hdt->get_heading();

                // If frame comes from real device => disable HDT creation
                if (to_string(sentence->get_talker()) != CUSTOM_TALKER_ID)
                    hdt_dev_exist = true;

                // Publish ROS msg
                std_msgs::Float32 msg;
                _hdt = head_true.value();
                msg.data = _hdt; _hdt_pub.publish(msg);

                // Update and publish projected sog
                this->publish_proj_sog();

                break;
            }

            case nmea::sentence_id::HDG : // HDG sentence (Magnetic heading from compass)
            {
                double head_true, head_mag;
                auto hdg = nmea::sentence_cast<nmea::hdg>(sentence.get());
                auto raw_head_mag = hdg->get_heading();
                auto mag_dev = hdg->get_magn_dev();
                auto mag_dev_hem = hdg->get_magn_dev_hem();

                // Compute magnetic bearing
                if (mag_dev_hem.value() == nmea::direction::east)
                    head_mag = static_cast<double>(raw_head_mag.value() + mag_dev.value());
                else
                    head_mag = static_cast<double>(raw_head_mag.value() - mag_dev.value());

                // Create hdt frame for heading true if no hdt received before and valid GPS frame rcved
                if (!hdt_dev_exist && gps_dev_exist)
                {
                    if (mag_dev_hem.value() == nmea::direction::east)
                        head_true = head_mag + _mag_dev;
                    else
                        head_true = head_mag + _mag_dev;

                    this->send_HDT_frame(head_true);
                }

                // Publish ROS msg
                std_msgs::Float32 msg;
                msg.data = head_mag;
                _hdg_pub.publish(msg);

                break;
            }

            default :
                break;
        }
    }
    catch (...)
    {
    }
}

void Driver_nmea::send_HDT_frame(double h_t)
{
    // Send hdt frame with gps dev
    nmea::hdt hdt;
    hdt.set_talker(nmea::make_talker(CUSTOM_TALKER_ID));
    hdt.set_heading(h_t);

    nmea_msgs::Sentence msg;
    msg.header.stamp = ros::Time::now();
    msg.header.frame_id = CUSTOM_TALKER_ID;
    msg.sentence = to_string(hdt);

    QVariant var; var.setValue(msg);
    publish_nmea0183_sentence(var);
}

void Driver_nmea::publish_proj_sog()
{
    std_msgs::Float32 p_sog;
    p_sog.data = cos(abs(_cog-_hdt))*_sog;
    _proj_sog_pub.publish(p_sog);
}

////////////////////////////
//// CALLBACK FUNCTIONS ////
////////////////////////////



///////////////////////////
//// PUBLISH FUNCTIONS ////
///////////////////////////

void Driver_nmea::publish_nmea0183_sentence(QVariant var)
{
    nmea_msgs::Sentence msg = var.value<nmea_msgs::Sentence>();

    // Publish raw NMEA
    _nmea0183_sentence_pub.publish(msg);

    this->parse_nmea(msg);
}

////////////////////////////
//// NMEA READER THREAD ////
////////////////////////////

Nmea_reader_thread::Nmea_reader_thread(std::string name)
{
    _thread_name = name;
}

void Nmea_reader_thread::run()
{
    // Dev serial 8N1
    default_nmea_reader reader{utils::make_unique<serial>(_dev_name.c_str(), _baud,
                                                          serial::databits::bit_8,
                                                          serial::stopbits::bit_1,
                                                          serial::parity::none)};

    while (ros::ok())
    {
        std::string data;

        try
        {
            while (reader.read_sentence(data))
            {
                ROS_DEBUG_STREAM(ros::this_node::getName() << " => data : " << data);

                auto sentence = nmea::make_sentence(data);

                nmea_msgs::Sentence msg;

                msg.header.stamp = ros::Time::now();
                msg.header.frame_id = to_string(sentence->get_talker());
                msg.sentence = to_string(*sentence);

                QVariant var; var.setValue(msg);
                emit(sentence_received(var));
            }
        }
        catch(const std::exception& e)
        {
            std::string err_str = e.what();

            // To avoid specific error display (GSA and GSV : NMEA4.1 not compatible with marnav NMEA2.3)
            if (err_str.find("invalid number of fields in gsa") == std::string::npos && err_str.find("invalid number of fields in gsv: expected 3+n*4, got ") == std::string::npos)
                ROS_ERROR("%s thread error : %s. No data will be available...",_thread_name.c_str(),err_str.c_str());

            ROS_DEBUG_STREAM(ros::this_node::getName() << " => data : " << data);
        }
        catch (...)
        {
            ROS_ERROR("%s thread error unknow. No data will be available...",_thread_name.c_str());
        }
    }
}

void Nmea_reader_thread::init(const std::string & dev_name, const int & baud)
{
    _dev_name = "/dev/" + dev_name;

    switch (baud)
    {
        case 300 :
        {
            _baud = serial::baud::baud_300;
            break;
        }
        case 600 :
        {
            _baud = serial::baud::baud_600;
            break;
        }
        case 1200 :
        {
            _baud = serial::baud::baud_1200;
            break;
        }
        case 2400 :
        {
            _baud = serial::baud::baud_2400;
            break;
        }
        case 4800 :
        {
            _baud = serial::baud::baud_4800;
            break;
        }
        case 9600 :
        {
            _baud = serial::baud::baud_9600;
            break;
        }
        case 19200 :
        {
            _baud = serial::baud::baud_19200;
            break;
        }
        case 38400 :
        {
            _baud = serial::baud::baud_38400;
            break;
        }
        case 57600 :
        {
            _baud = serial::baud::baud_57600;
            break;
        }
        case 115200 :
        {
            _baud = serial::baud::baud_115200;
            break;
        }
        case 230400 :
        {
            _baud = serial::baud::baud_230400;
            break;
        }
        default :
        {
            _baud = serial::baud::baud_4800;
            break;
        }
    }
}
