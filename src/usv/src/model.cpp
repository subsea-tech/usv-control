#include "model.h"

Parameters::Parameters()
{

}

Parameters::~Parameters()
{

}

double Parameters::get_fx_max()
{
    return 2*cT*rpm_max*rpm_max;
}

double Parameters::get_tz_max()
{
    return l*cT*rpm_max*rpm_max;
}

Model::Model(Parameters * p) : QObject()
{
    rpm_left = 0;
    rpm_right = 0;

    fx = 0;
    tz = 0;

    _param = p;
}

Model::~Model()
{

}

void Model::compute_rpm_order()
{
    rpm_left = static_cast<int16_t>(round(sign(_param->l/2.0*fx-tz)*sqrt(abs(_param->l/2.0*fx-tz)/(_param->l*_param->cT))));
    rpm_right = static_cast<int16_t>(round(sign(_param->l/2.0*fx+tz)*sqrt(abs(_param->l/2.0*fx+tz)/(_param->l*_param->cT))));

    // Saturations
    if (rpm_left > _param->rpm_max)
        rpm_left = _param->rpm_max;
    else if (rpm_left < -_param->rpm_max)
        rpm_left = -_param->rpm_max;
    if (rpm_right > _param->rpm_max)
        rpm_right = _param->rpm_max;
    else if (rpm_right < -_param->rpm_max)
        rpm_right = -_param->rpm_max;
}
