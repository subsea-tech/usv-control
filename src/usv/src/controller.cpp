#include "controller.h"

Autohead::Autohead(Parameters * p) : QObject()
{
    active = false;

    // Set default parameters
    this->set_def_param();

    psi_des = 0;
    psi_mes = 0;
    t_out = 0;

    err = 0;
    dpsi_fil = 0;
    p_val = 0;
    i_val = 0;
    d_val = 0;

    _param = p;

    _integrator = 0;

    _n_psi_fil = 83;
    _fc_psi_fil = 1;

    _n_fil_dot = 1;

    this->read_settings();
}

Autohead::~Autohead()
{
}

void Autohead::init()
{
    _ctrl_period = _param->dt_ctrl_ms/1000.0;
    this->synthetize_dpsi_fir();
}

void Autohead::set_def_param()
{
    kp = KP_Z;
    ki = KI_Z;
    i_sat_in = I_SAT_IN_Z;
    i_sat_out = I_SAT_OUT_Z;
    i_omega_thresh = I_THRESH_Z;
    kd = KD_Z;
}

void Autohead::read_settings()
{
    QSettings settings(SETTINGS_FOLDER, "autohead_ctrler");

    kp = settings.value("kp",kp).toDouble();
    ki = settings.value("ki",ki).toDouble();
    i_sat_in = settings.value("i_sat_in",i_sat_in).toDouble();
    i_sat_out = settings.value("i_sat_out",i_sat_out).toDouble();
    i_omega_thresh = settings.value("i_omega_thresh",i_omega_thresh).toDouble();
    kd = settings.value("kd",kd).toDouble();
    _n_psi_fil = settings.value("n_dpsi_fil",_n_psi_fil).toUInt();
    _fc_psi_fil = settings.value("fc_dpsi_fil",_fc_psi_fil).toDouble();

    this->write_settings(); // Write Settings if keys not already exist
}

void Autohead::write_settings()
{
    QSettings settings(SETTINGS_FOLDER, "autohead_ctrler");

    settings.setValue("kp",kp);
    settings.setValue("ki",ki);
    settings.setValue("i_sat_in",i_sat_in);
    settings.setValue("i_sat_out",i_sat_out);
    settings.setValue("i_omega_thresh",i_omega_thresh);
    settings.setValue("kd",kd);
    settings.setValue("n_dpsi_fil",_n_psi_fil);
    settings.setValue("fc_dpsi_fil",_fc_psi_fil);
}

void Autohead::start()
{
    this->reset_int();

    active = true;
    _dpsi_mes_q.clear();
    _derr_q.clear();

    psi_des = psi_mes;

    emit(started());
}

void Autohead::stop()
{
    active = false;

    t_out = 0;
    err = 0;
    p_val = 0;
    i_val = 0;
    d_val = 0;
}

void Autohead::reset_int()
{
    _integrator = 0;
}

void Autohead::set_head_des(double head)
{
    // Convert in [0 2.PI] rad (Warning trigo and bearing not same dir)
    double p_tmp = fmod(-head,360.0)*D2R;
    if (p_tmp < 0)
        p_tmp += 2*PI;

    // Extract nb of round
    int32_t rd = static_cast<int32_t>(floor(psi_mes/(2*PI)));
    p_tmp += rd*2*PI;               // psi_des + rd

    if (p_tmp-psi_mes > PI)         // Select previous round
        psi_des = p_tmp - 2*PI;
    else if (p_tmp-psi_mes < -PI)   // Next round
        psi_des = p_tmp + 2*PI;
    else
        psi_des = p_tmp;
}

void Autohead::set_head_mes(double head)
{
    // Convert in [0 2.PI] rad (Warning trigo and bearing not same dir)
    double p_tmp = fmod(-head,360.0)*D2R;
    if (p_tmp < 0)
        p_tmp += 2*PI;

    // Extract nb of round
    int32_t rd = static_cast<int32_t>(floor(psi_mes/(2*PI)));
    p_tmp += rd*2*PI;               // psi_mes + rd

    if (p_tmp-psi_mes > PI)         // Select previous round
        psi_mes = p_tmp - 2*PI;
    else if (p_tmp-psi_mes < -PI)   // Next round
        psi_mes = p_tmp + 2*PI;
    else
        psi_mes = p_tmp;
}

float Autohead::get_err_deg()
{
    float err_deg = fmod(-err*180./PI,360);

    if (err_deg > 180)
        err_deg -= 360;
    else if (err_deg < -180)
        err_deg += 360;

    return err_deg;
}

void Autohead::update()
{
    // Error
    err = psi_des - psi_mes;

    // Omega estimation
    dpsi_fil = this->dpsi_filter();

    // Ctrl
    if (active)
    {
        // Normalized error
        double err_norm = err * _param->Iz / _ctrl_period;
        double err_speed = 0 - dpsi_fil;

        // Omega threshold, to hold integrator
        if ( abs(dpsi_fil) < i_omega_thresh*D2R )
        {
            // Int error and input saturation
            if (err_norm > i_sat_in)
                _integrator += ki*i_sat_in*_ctrl_period;    // BW Euler
            else if (err_norm < -i_sat_in)
                _integrator += -ki*i_sat_in*_ctrl_period;
            else
                _integrator += ki*err_norm*_ctrl_period;

            // Integrator saturation
            if (_integrator > i_sat_out)
                _integrator = i_sat_out;
            else if (_integrator < -i_sat_out)
                _integrator = -i_sat_out;
        }

        // Ctrler output : Torque Z (in N.m)
        p_val = kp*err_norm;
        i_val = _integrator;
        d_val = kd*err_speed;
        t_out = p_val + i_val + d_val;
    }
    else
    {
        this->reset_int();
        t_out = 0;
    }
}

double Autohead::compute_dot()
{
    // Compute derivation error
    static double psi_mes_prev = psi_mes;
    double derr = (psi_mes - psi_mes_prev)/_ctrl_period;
    psi_mes_prev = psi_mes;

    // Push last arrived
    if (_derr_q.size() >= _n_fil_dot)
        _derr_q.pop_front();
    _derr_q.push_back(derr);

    // Median filter
    deque<double> tmp(_derr_q);
    nth_element(tmp.begin(), tmp.begin() + tmp.size()/2, tmp.end());
    double derr_fil = tmp[tmp.size()/2];

    return derr_fil;
}

double Autohead::dpsi_filter()
{
    static double psi_mes_prev = psi_mes;

    // Update queue psi_mes
    while (_dpsi_mes_q.size() >= _n_psi_fil)
        _dpsi_mes_q.pop_front();
    _dpsi_mes_q.push_back((psi_mes-psi_mes_prev)/_ctrl_period);
    psi_mes_prev = psi_mes;

    // Filtering
    double dpsi_fil_temp = 0;
    if (_dpsi_mes_q.size() == _n_psi_fil)
    {
        for (unsigned int i = 0 ; i < _n_psi_fil ; i++)
            dpsi_fil_temp += _h_psi_fil[i]*_dpsi_mes_q[i];
    }

    // Update psi_mes value
    return dpsi_fil_temp;
}

void Autohead::synthetize_dpsi_fir()
{
    _h_psi_fil.resize(_n_psi_fil);

    double a = _fc_psi_fil * _ctrl_period;
    unsigned int P = (_n_psi_fil - 1)/2; // Middle of filter
    for (unsigned int i = 0 ; i < _n_psi_fil ; i++)
    {
        double hanning_win = cos(PI*int(i-P)/P)*cos(PI*int(i-P)/P);
        if (i == P)
            _h_psi_fil[i] = 2*a;
        else
            _h_psi_fil[i] = 2*a*sin(2*PI*int(i-P)*a)/(2*PI*int(i-P)*a)*hanning_win;
    }
}

Autospeed::Autospeed(Parameters * p) : QObject()
{
    active = false;

    this->set_def_param();

    v_des = 0;
    v_mes = 0;

    err = 0;
    ff_val = 0;
    p_val = 0;
    i_val = 0;

    f_out = 0;

    _param = p;

    _integrator = 0;

    this->read_settings();
}

Autospeed::~Autospeed()
{
}

void Autospeed::init()
{
    _ctrl_period = _param->dt_ctrl_ms/1000.0;
}

void Autospeed::set_def_param()
{
    kff = KFF_V;
    kp = KP_V;
    ki = KI_V;
    i_sat = I_SAT_V;
}

void Autospeed::read_settings()
{
    QSettings settings(SETTINGS_FOLDER, "autospeed_ctrler");

    kff = settings.value("kff",kff).toDouble();
    kp = settings.value("kp",kp).toDouble();
    ki = settings.value("ki",ki).toDouble();
    i_sat = settings.value("i_sat",i_sat).toDouble();

    this->write_settings(); // Write Settings if keys not already exist
}

void Autospeed::write_settings()
{
    QSettings settings(SETTINGS_FOLDER, "autospeed_ctrler");

    settings.setValue("kff",kff);
    settings.setValue("kp",kp);
    settings.setValue("ki",ki);
    settings.setValue("i_sat",i_sat);
}

void Autospeed::start()
{
    this->reset_int();

    active = true;

    v_des = 0;

    emit(started());
}

void Autospeed::stop()
{
    active = false;

    f_out = 0;
    err = 0;
    ff_val = 0;
    p_val = 0;
    i_val = 0;
}

void Autospeed::reset_int()
{
    _integrator = 0;
}

void Autospeed::set_v_des(double v_kt)
{
    v_des = v_kt*KT2MS;
}

void Autospeed::set_v_mes(double v_kt)
{
    v_mes = v_kt*KT2MS;
}

void Autospeed::update()
{
    err = v_des - v_mes;

    if (active)
    {
        // Normalized error
        double err_norm = err/_ctrl_period*_param->m;

        // Int error
        _integrator += ki*err_norm*_ctrl_period; // BW Euler

        // Integrator saturation
        if (_integrator > i_sat)
            _integrator = i_sat;
        else if (_integrator < -i_sat)
            _integrator = -i_sat;

        // Ctrler output : force FW (in N)
        ff_val = kff*v_des;
        p_val = kp*err_norm;
        i_val = _integrator;
        f_out = ff_val + p_val + i_val;
    }
    else
    {
        this->reset_int();
        f_out = 0;
    }
}
