#include "usv_ctrl.h"

static Usv_ctrl *usv_ctrl;

void sig_handler [[ noreturn ]] (int s)
{
    std::cout << "usv_ctrl : Caught signal " << s << std::endl;

    usv_ctrl->quit();

	exit(s);
}

int main(int argc, char *argv[])
{
	ros::init(argc, argv, "usv_ctrl", ros::init_options::NoSigintHandler);
    ros::NodeHandle nh("~");

	signal(SIGINT,sig_handler);
	signal(SIGTERM,sig_handler);

    usv_ctrl = new Usv_ctrl(argc,argv,&nh);

    return usv_ctrl->exec();
}


Usv_ctrl::Usv_ctrl(int argc, char **argv, ros::NodeHandle * nh) : QCoreApplication(argc,argv), _joyctrl(&_p), _autohead(&_p), _autospeed(&_p), _model(&_p)
{
    // Init members
    _Nh = nh;

    _gps.valid = false;

    _nav_ctrl_cmd.mode_ctrl = nav_ctrl_cmd::MODE_CTRL_IDLE;
    _nav_ctrl_cmd.speed_order = 0;
    _nav_ctrl_cmd.heading_order = 0;

    _esc_left_cmd.speed = 0;
    _esc_left_cmd.enable = false;
    _esc_left_cmd.power_limit = 0;
    _esc_right_cmd.speed = 0;
    _esc_right_cmd.enable = false;
    _esc_right_cmd.power_limit = 0;

    // Publishers
    _esc_right_pub = _Nh->advertise<msg_usv::esc_cmd>("esc_right_cmd", 10);
    _esc_left_pub = _Nh->advertise<msg_usv::esc_cmd>("esc_left_cmd", 10);
    _ctrl_val_pub = _Nh->advertise<msg_usv::ctrl_values>("ctrl_values", 10);
    _ctrl_param_pub = _Nh->advertise<msg_usv::ctrl_param>("ctrl_param", 10);
    _nav_ctrl_pub = _Nh->advertise<msg_usv::nav_ctrl_cmd>("nav_ctrl_cmd", 10);

    // Subscribers
    _nmea0183_sentence_sub = _Nh->subscribe("/nmea0183_sentence", 10, &Usv_ctrl::nmea0183_sentence_callback,this);
    std::setlocale(LC_ALL, "C"); // Very important : chg locale to use marnav library
    _joy_sub = _Nh->subscribe("joy", 10, &Usv_ctrl::joy_callback,this);
    _nav_ctrl_sub = _Nh->subscribe("nav_ctrl_cmd", 10, &Usv_ctrl::nav_ctrl_cmd_callback,this);
    _ctrl_param_sub = _Nh->subscribe("ctrl_param", 10, &Usv_ctrl::ctrl_param_callback,this);
    _enable_sub = _Nh->subscribe("enable", 10, &Usv_ctrl::enable_callback,this);

    // ROS Parameters (init Parameters class)
    std::string param_name;
    param_name = "m";
    if (!_Nh->hasParam(param_name)) // Create param if doesn't exist
        _Nh->setParam(param_name,CTRL_M);
    _Nh->getParam(param_name,_p.m);

    param_name = "Iz";
    if (!_Nh->hasParam(param_name)) // Create param if doesn't exist
        _Nh->setParam(param_name,CTRL_IZ);
    _Nh->getParam(param_name,_p.Iz);

    param_name = "cT";
    if (!_Nh->hasParam(param_name)) // Create param if doesn't exist
        _Nh->setParam(param_name,CTRL_CT);
    _Nh->getParam(param_name,_p.cT);

    param_name = "rpm_max";
    if (!_Nh->hasParam(param_name)) // Create param if doesn't exist
        _Nh->setParam(param_name,CTRL_RPM_MAX);
    _Nh->getParam(param_name,_p.rpm_max);

    param_name = "l";
    if (!_Nh->hasParam(param_name)) // Create param if doesn't exist
        _Nh->setParam(param_name,CTRL_L);
    _Nh->getParam(param_name,_p.l);

    param_name = "dt_ctrl_ms";
    if (!_Nh->hasParam(param_name)) // Create param if doesn't exist
        _Nh->setParam(param_name,CTRL_DT_CTRL);
    _Nh->getParam(param_name,_p.dt_ctrl_ms);

    // Init ctrlers (to update param values)
    _autohead.init();
    _autospeed.init();

    // Timers
    connect(&_timer_ros, &QTimer::timeout, this, [=](){ ros::spinOnce(); });
    connect(&_timer_ctrl, SIGNAL(timeout()), this, SLOT(update_ctrl()));
    connect(&_gps_wd, &QTimer::timeout, this, [=](){ if (_nav_ctrl_cmd.mode_ctrl != nav_ctrl_cmd::MODE_CTRL_JOY && _nav_ctrl_cmd.mode_ctrl != nav_ctrl_cmd::MODE_CTRL_IDLE)
                                                        {   ROS_ERROR("[USV ctrl] : GPS time out!");
                                                            this->publish_ctrl_mode(nav_ctrl_cmd::MODE_CTRL_IDLE);    } }); // Stop Ctrl if automode
    connect(&_compass_wd, &QTimer::timeout, this, [=](){ if (_nav_ctrl_cmd.mode_ctrl != nav_ctrl_cmd::MODE_CTRL_JOY && _nav_ctrl_cmd.mode_ctrl != nav_ctrl_cmd::MODE_CTRL_IDLE)
                                                        {   ROS_ERROR("[USV ctrl] : Compass time out!");
                                                            this->publish_ctrl_mode(nav_ctrl_cmd::MODE_CTRL_IDLE);    } }); // Stop Ctrl if automode
    connect(&_nav_frame_wd, &QTimer::timeout, this, [=](){ if (_nav_ctrl_cmd.mode_ctrl == nav_ctrl_cmd::MODE_CTRL_AUTOPILOT)
                                                        {   ROS_INFO("[USV ctrl] : RMB sentence not received (time out)! => Keep position");
                                                            this->publish_ctrl_mode(nav_ctrl_cmd::MODE_CTRL_KEEP_POS); } }); // Keep Pos if RMB time-out
    _timer_ros.start(TIME_MS_ROS_SPIN);
    _timer_ctrl.start(_p.dt_ctrl_ms);
    _timer_ctrl.setTimerType(Qt::PreciseTimer);
    _gps_wd.setInterval(TIME_OUT_MS_NMEA); _gps_wd.start();
    _compass_wd.setInterval(TIME_OUT_MS_NMEA); _compass_wd.start();
    _nav_frame_wd.setInterval(TIME_OUT_MS_NMEA);
}

////////////////////////////
//// CALLBACK FUNCTIONS ////
////////////////////////////

void Usv_ctrl::nmea0183_sentence_callback(nmea_msgs::Sentence msg)
{
    // GPS, AIS, Gyrocompas, radar
    std::string nmea_str = msg.sentence;

    try
    {
        // NMEA parse
        auto sentence = nmea::make_sentence(nmea_str);

        switch (sentence->id())
        {
            case nmea::sentence_id::RMC : // RMC sentence (GPS)
            {
                auto rmc = nmea::sentence_cast<nmea::rmc>(sentence.get());
                auto status = rmc->get_status();
                auto lat = rmc->get_lat();
                auto lon = rmc->get_lon();
                auto date = rmc->get_date();
                auto time_utc = rmc->get_time_utc();
                auto head_gps = rmc->get_heading();
                auto sog = rmc->get_sog().value();

                QGeoCoordinate coord(lat->get(),lon->get());
                QDate qdate(static_cast<int>(date->year()),static_cast<int>(date->mon()),static_cast<int>(date->day()));
                QTime qtime_utc(static_cast<int>(time_utc->hour()),static_cast<int>(time_utc->minutes()),static_cast<int>(time_utc->seconds()));
                _gps.pos_info = QGeoPositionInfo(coord,QDateTime(qdate,qtime_utc));
                _gps.sog = sog.get<units::knots>().value();
                _gps.cog = head_gps.value();

                // If valid => reset watch dog
                if (status.value() == 'A')
                    _gps_wd.start();

                break;
            }

            case nmea::sentence_id::HDT : // HDT sentence (Gyrocompas)
            {
                auto hdt = nmea::sentence_cast<nmea::hdt>(sentence.get());
                auto head_true = hdt->get_heading();
                _heading_true = head_true.value();

                // Reset watch dog
                _compass_wd.start();

                break;
            }

            case nmea::sentence_id::RMB : // RMB nav sentence
            {
                auto rmb = nmea::sentence_cast<nmea::rmb>(sentence.get());

                // Update desired position
                if (_nav_ctrl_cmd.mode_ctrl == nav_ctrl_cmd::MODE_CTRL_AUTOPILOT)
                {
                    _nav_ctrl_cmd.position_goto_order.latitude = rmb->get_lat().value();
                    _nav_ctrl_cmd.position_goto_order.longitude = rmb->get_lon().value();
                    _gpsctrl.set_pos_des(_nav_ctrl_cmd.position_goto_order);
                }

                // Reset wd
                _nav_frame_wd.start();

                break;
            }

            default :
                break;
        }
    }
    catch (...)
    {

    }
}

void Usv_ctrl::joy_callback(sensor_msgs::Joy msg)
{
    if (_enable)
    {
        // Active Joy ctrl mode
        this->publish_ctrl_mode(nav_ctrl_cmd::MODE_CTRL_JOY);

        // Axes update
        if (msg.axes.size() >= 2)
        {
            _joyctrl.joy_fw = static_cast<double>(msg.axes[1]);
            _joyctrl.joy_lr = static_cast<double>(-msg.axes[0]);
        }
        // Buttons update
        if (msg.buttons.size() >= 2)
        {
            if (msg.buttons[0] > 0)
            {
                // Increase k1 parameters by 10%
                if ( (_joyctrl.k1 += 10) > 100)
                    _joyctrl.k1 = -100;

                // Publish parameters
                this->publish_ctrl_param();
            }
            if (msg.buttons[1] > 0)
            {
                // Increase k2 parameters by 10%
                if ( (_joyctrl.k2 += 10) > 100)
                    _joyctrl.k2 = -100;

                // Publish parameters
                this->publish_ctrl_param();
            }
        }
    }
}

void Usv_ctrl::nav_ctrl_cmd_callback(msg_usv::nav_ctrl_cmd msg)
{
    if (_enable)
    {
        _nav_ctrl_cmd = msg;
        this->update_ctrl_mode(msg.mode_ctrl);
    }
}

void Usv_ctrl::ctrl_param_callback(const ros::MessageEvent<msg_usv::ctrl_param const>& event)
{
    if (event.getPublisherName() != ros::this_node::getName()) // If publisher is not itself
    {
        const msg_usv::ctrl_paramConstPtr& msg = event.getMessage();

        if (msg->save_param != msg_usv::ctrl_param::PARAM_LOAD) // If load not asked
        {
            _joyctrl.k1 = msg->k1_joy;
            _joyctrl.k2 = msg->k2_joy;
            _joyctrl.mode = static_cast<JoyMode::EJoyMode>(msg->mode_joy);

            _autospeed.kff = static_cast<double>(msg->kff_v);
            _autospeed.kp = static_cast<double>(msg->kp_v);
            _autospeed.ki = static_cast<double>(msg->ki_v);
            _autospeed.i_sat = static_cast<double>(msg->isat_v);

            _autohead.kp = static_cast<double>(msg->kp_z);
            _autohead.ki = static_cast<double>(msg->ki_z);
            _autohead.kd = static_cast<double>(msg->kd_z);
            _autohead.i_sat_in = static_cast<double>(msg->isat_in_z);
            _autohead.i_sat_out = static_cast<double>(msg->isat_out_z);
            _autohead.i_omega_thresh = static_cast<double>(msg->ithresh_z);

            _gpsctrl.head_err = msg->head_err;
            _gpsctrl.dist_stop = msg->d_stop;
            _gpsctrl.dist_slow = msg->d_slow;
            _gpsctrl.power_speed_fast = msg->pw_fast;
            _gpsctrl.power_speed_slow = msg->pw_slow;

            switch (msg->save_param)
            {
                case  msg_usv::ctrl_param::PARAM_SAVE : // Save parameters
                {
                    _autospeed.write_settings();
                    _autohead.write_settings();
                    _joyctrl.write_settings();
                    _gpsctrl.write_settings();

                    break;
                }
                case msg_usv::ctrl_param::PARAM_RESET_JOY : // Reset joy parameters to default values
                {
                    _joyctrl.set_def_param();
                    _joyctrl.write_settings();

                    break;
                }
                case msg_usv::ctrl_param::PARAM_RESET_AUTOSPEED : // Reset autospeed parameters to default values
                {
                    _autospeed.set_def_param();
                    _autospeed.write_settings();

                    break;
                }
                case msg_usv::ctrl_param::PARAM_RESET_AUTOHEAD : // Reset autohead parameters to default values
                {
                    _autohead.set_def_param();
                    _autohead.write_settings();

                    break;
                }
                case msg_usv::ctrl_param::PARAM_RESET_AUTOGPS : // Reset gps ctrler parameters to default values
                {
                    _gpsctrl.set_def_param();
                    _gpsctrl.write_settings();

                    break;
                }
                case msg_usv::ctrl_param::PARAM_RESET_ALL : // Reset all ctrler parameters to default values
                {
                    _joyctrl.set_def_param();
                    _joyctrl.write_settings();
                    _autospeed.set_def_param();
                    _autospeed.write_settings();
                    _autohead.set_def_param();
                    _autohead.write_settings();
                    _gpsctrl.set_def_param();
                    _gpsctrl.write_settings();

                    break;
                }
                default :
                {

                    break;
                }
            }
        }

        // Send new param
        this->publish_ctrl_param();
    }
}

void Usv_ctrl::enable_callback(const std_msgs::Bool msg)
{
    if (msg.data && _enable == false)
    {
        ROS_INFO("[USV ctrl] : Controller enable");

        _enable = true;

        // Enable Esc
        this->enable_esc(true);

        // Reset ctrl mode
        this->publish_ctrl_mode(nav_ctrl_cmd::MODE_CTRL_IDLE);
    }
    else
    {
        ROS_INFO("[USV ctrl] : Controller disable");

        _enable = false;

        // Disable ESC
        this->enable_esc(false);

        // Stop controllers
        this->stop_ctrlers();

        // Reset ctrl mode
        this->publish_ctrl_mode(nav_ctrl_cmd::MODE_CTRL_IDLE);
    }
}

///////////////////////////
//// PUBLISH FUNCTIONS ////
///////////////////////////

void Usv_ctrl::publish_esc_cmd()
{
    _esc_left_pub.publish(_esc_left_cmd);
    _esc_right_pub.publish(_esc_right_cmd);
}

void Usv_ctrl::publish_ctrl_values()
{
    msg_usv::ctrl_values m;

    // GPS values
    m.pos_des = _gpsctrl.get_pos_des();
    m.pos_mes = _gpsctrl.get_pos_mes();
    m.gps_dist = _gpsctrl.dist;
    m.gps_head = _gpsctrl.head_out;

    // Joy values
    m.joy_fw = static_cast<float>(_joyctrl.joy_fw*100);
    m.joy_lr = static_cast<float>(_joyctrl.joy_lr*100);

    // Auto-speed values
    m.v_des = static_cast<float>(_autospeed.v_des);
    m.v_mes = static_cast<float>(_autospeed.v_mes);
    m.err_v = static_cast<float>(_autospeed.err);
    m.ff_v = static_cast<float>(_autospeed.ff_val);
    m.p_v = static_cast<float>(_autospeed.p_val);
    m.i_v = static_cast<float>(_autospeed.i_val);

    // Auto-head values
    m.psi_des = static_cast<float>(_autohead.psi_des);
    m.psi_mes = static_cast<float>(_autohead.psi_mes);
    m.err_z = static_cast<float>(_autohead.err);
    m.dpsi_fil = static_cast<float>(_autohead.dpsi_fil);
    m.p_z = static_cast<float>(_autohead.p_val);
    m.i_z = static_cast<float>(_autohead.i_val);
    m.d_z = static_cast<float>(_autohead.d_val);

    // Input model
    m.f_x = static_cast<float>(_model.fx);
    m.t_z = static_cast<float>(_model.tz);

    // Publish
    _ctrl_val_pub.publish(m);
}

void Usv_ctrl::publish_ctrl_param()
{
    msg_usv::ctrl_param m;

    m.save_param = msg_usv::ctrl_param::PARAM_NOTHING;
    m.k1_joy = static_cast<short>(_joyctrl.k1);
    m.k2_joy = static_cast<short>(_joyctrl.k2);
    m.mode_joy = static_cast<short>(_joyctrl.mode);
    m.kff_v = static_cast<float>(_autospeed.kff);
    m.kp_v = static_cast<float>(_autospeed.kp);
    m.ki_v = static_cast<float>(_autospeed.ki);
    m.isat_v = static_cast<float>(_autospeed.i_sat);
    m.kp_z = static_cast<float>(_autohead.kp);
    m.ki_z = static_cast<float>(_autohead.ki);
    m.kd_z = static_cast<float>(_autohead.kd);
    m.isat_in_z = static_cast<float>(_autohead.i_sat_in);
    m.isat_out_z = static_cast<float>(_autohead.i_sat_out);
    m.ithresh_z = static_cast<float>(_autohead.i_omega_thresh);
    m.head_err = static_cast<float>(_gpsctrl.head_err);
    m.d_stop = static_cast<float>(_gpsctrl.dist_stop);
    m.d_slow = static_cast<float>(_gpsctrl.dist_slow);
    m.pw_fast = static_cast<unsigned short>(_gpsctrl.power_speed_fast);
    m.pw_slow = static_cast<unsigned short>(_gpsctrl.power_speed_slow);

    // Publish
    _ctrl_param_pub.publish(m);
}

void Usv_ctrl::publish_ctrl_mode(uint16_t mode)
{
    msg_usv::nav_ctrl_cmd m;
    m.mode_ctrl = mode;
    m.position_goto_order = _nav_ctrl_cmd.position_goto_order;

    _nav_ctrl_pub.publish(m);
}

/////////////////////////
//// OTHER FUNCTIONS ////
/////////////////////////

void Usv_ctrl::stop_ctrlers()
{
    // Disable ctrlers
    _autohead.stop();
    _autospeed.stop();

    // Reset Outputs
    _model.fx = 0;
    _model.tz = 0;

    // Reset set points
    _esc_left_cmd.speed = 0;
    _esc_right_cmd.speed = 0;

    // Publish
    this->publish_esc_cmd();
    this->publish_ctrl_values();
}

void Usv_ctrl::enable_esc(bool on)
{
    _esc_left_cmd.enable = on;
    _esc_right_cmd.enable = on;
}

void Usv_ctrl::update_ctrl()
{
    // Update measures
    _autohead.set_head_mes(_heading_true);
    _autospeed.set_v_mes(_gps.sog*cos((_heading_true-_gps.cog)*D2R));
    _gpsctrl.set_pos_mes(_gps.pos_info);

    // Update RPM for selected mode
    switch (_nav_ctrl_cmd.mode_ctrl)
    {
        case nav_ctrl_cmd::MODE_CTRL_JOY :
        {
            // Update ctrler
            _joyctrl.update();

            // Update model inputs
            _model.fx = _joyctrl.f_out;
            _model.tz = _joyctrl.t_out;

            break;
        }
        case nav_ctrl_cmd::MODE_CTRL_GPS :
        case nav_ctrl_cmd::MODE_CTRL_AUTOPILOT :
        {
            // Update target distance and bearing
            _gpsctrl.update();

            ROS_DEBUG("[%s] : Autopilot/Gps ctrl, dist : %f, max : %f",ros::this_node::getName().c_str(),_gpsctrl.dist,_gpsctrl.dist_stop);

            // Update model inputs
            if (_gpsctrl.dist <= _gpsctrl.dist_stop) // If inside of specified radius
            {
                if (_nav_ctrl_cmd.mode_ctrl == nav_ctrl_cmd::MODE_CTRL_GPS)
                    this->publish_ctrl_mode(nav_ctrl_cmd::MODE_CTRL_KEEP_POS); // When arrived at WP Keep position in GPS Mode
                else
                    this->stop_ctrlers(); // Wait next rmb frame in Autopilot Mode
            }
            else
            {
                // Start controllers (if not active)
                if (!_autohead.isActive())
                    _autohead.start();

                // Update head ctrler
                _autohead.set_head_des(round(_gpsctrl.head_out));
                _autohead.update();

                // Update vertical torque
                _model.tz = _autohead.t_out;

                // Update FW order
                if (abs(_autohead.err*180./PI) > _gpsctrl.head_err) // If heading error > thresh => No forward
                    _model.fx = 0;
                else
                {
                    if (_gpsctrl.dist > _gpsctrl.dist_slow) // Far from WP => cruising speed and condition on heading for FW
                        _model.fx = _p.get_fx_max()*_gpsctrl.power_speed_fast/100.;
                    else
                        _model.fx = _p.get_fx_max()*_gpsctrl.power_speed_slow/100.;
                }
            }

            break;
        }
        case nav_ctrl_cmd::MODE_CTRL_KEEP_POS :
        {
            // Update gpsctrl
            _gpsctrl.update();

            if (_gpsctrl.dist <= _gpsctrl.dist_stop) // If inside of specified radius
                this->stop_ctrlers();
            else
            {
                // Start controller (if not active)
                if (!_autohead.isActive())
                    _autohead.start();

                // Error in deg [-180; 180]
                double err = static_cast<int>(_heading_true - round(_gpsctrl.head_out))%360;
                if (err > 180)
                    err -= 360;
                else if (err < -180)
                    err += 360;

                // Update head ctrler
                if (abs(err) > 90) // if wp is close to the back of the usv
                    _autohead.set_head_des((static_cast<int>(round(_gpsctrl.head_out))+180)%360);
                else
                    _autohead.set_head_des(round(_gpsctrl.head_out));
                _autohead.update();

                // Update vertical torque
                _model.tz = _autohead.t_out;

                // Update FW force (proportional to dist) 10m <=> power_speed_slow
                _model.fx = _gpsctrl.dist/10*_p.get_fx_max()*_gpsctrl.power_speed_slow/100;

                // if wp closed to usv back
                if (abs(err) > 90)
                    _model.fx = -_model.fx;
            }

            break;
        }
        case nav_ctrl_cmd::MODE_CTRL_HEAD :
        {
            // Start ctrler
            if (!_autohead.isActive())
                _autohead.start();

            // Update set points
            _autohead.set_head_des(static_cast<double>(round(_nav_ctrl_cmd.heading_order)));
            _autospeed.set_v_des(static_cast<double>(_nav_ctrl_cmd.speed_order));

            // Update ctrlers
            _autohead.update();
            _autospeed.update();

            // Update model inputs
            _model.fx = _autospeed.f_out;
            _model.tz = _autohead.t_out;

            break;
        }
        default :
            break;
    }

    // Compute ESC cmd
    if (_nav_ctrl_cmd.mode_ctrl != nav_ctrl_cmd::MODE_CTRL_IDLE)
    {
        // Compute/Update RPM
        _model.compute_rpm_order();
        _esc_left_cmd.speed = _model.rpm_left;
        _esc_right_cmd.speed = _model.rpm_right;
    }
    else
    {
        _esc_left_cmd.speed = 0;
        _esc_right_cmd.speed = 0;
    }

    // Publish to ESC drivers
    this->publish_esc_cmd();

    // Publish ctrl values
    this->publish_ctrl_values();
}

void Usv_ctrl::update_ctrl_mode(uint16_t mode_ctrl)
{
    static uint16_t mode_ctrl_prev = 255;

    if (mode_ctrl_prev != mode_ctrl)
    {
        // Update mode
        mode_ctrl_prev = mode_ctrl;

        // Stop Auto-ctrlers
        this->stop_ctrlers();

        // Stop WD
        if (_nav_frame_wd.isActive())
            _nav_frame_wd.stop();

        // Enable/Disable ESC
        if (mode_ctrl == nav_ctrl_cmd::MODE_CTRL_IDLE)
            this->enable_esc(false);
        else
            this->enable_esc(true);

        switch (mode_ctrl)
        {
            case nav_ctrl_cmd::MODE_CTRL_AUTOPILOT :
            {
                ROS_INFO("[USV ctrl] : Mode control changed : AUTOPILOT");

                // Start watch dog
                _nav_frame_wd.start();

                break;
            }
            case nav_ctrl_cmd::MODE_CTRL_JOY :
            {
                ROS_INFO("[USV ctrl] : Mode control changed : JOY");

                break;
            }
            case nav_ctrl_cmd::MODE_CTRL_GPS :
            {
                ROS_INFO("[USV ctrl] : Mode control changed : GPS (%f , %f)",_nav_ctrl_cmd.position_goto_order.latitude,_nav_ctrl_cmd.position_goto_order.longitude);

                // Update gps goto order
                _gpsctrl.set_pos_des(_nav_ctrl_cmd.position_goto_order);

                break;
            }
            case nav_ctrl_cmd::MODE_CTRL_KEEP_POS :
            {
                // Update gps goto order
                _nav_ctrl_cmd.position_goto_order = _gpsctrl.get_pos_mes();
                _gpsctrl.set_pos_des(_nav_ctrl_cmd.position_goto_order);

                ROS_INFO("[USV ctrl] : Mode control changed : KEEP POS (%f, %f)",_nav_ctrl_cmd.position_goto_order.latitude,_nav_ctrl_cmd.position_goto_order.longitude);

                // Publish keep_position to share position goto order
                this->publish_ctrl_mode(nav_ctrl_cmd::MODE_CTRL_KEEP_POS);

                break;
            }
            case nav_ctrl_cmd::MODE_CTRL_HEAD :
            {
                ROS_INFO("[USV ctrl] : Mode control changed : HEAD");

                break;
            }
            case nav_ctrl_cmd::MODE_CTRL_IDLE :
            {
                ROS_INFO("[USV ctrl] : Mode control changed : IDLE");

                // Publish
                this->publish_esc_cmd();

                break;
            }
        }
    }
}
