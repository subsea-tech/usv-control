# USV Sources

## Clone sources
>git clone https://valentin_riviere@bitbucket.org/valentin_riviere/usv-control.git

## ROS Packages and contains
- `msg_usv` : Messages used for USV
- `usv` : All nodes used for USV control
- `scripts` : Contains scripts to interface openCPN

## USV nodes
- `usv_ctrl` : To control usv
	- `motor_right_rev` : Right motor direction
	- `motor_left_rev` : Left motor direction
- `driver_nmea` : To capture NMEA frame on serial port
	- `gps_dev` : GPS device (/dev/tty*)
	- `gps_baud` : GPS baudrate (default 4800)
	- `compass_dev` : Compass device (/dev/tty*)
	- `compass_baud` : Compass baudrate (default 4800)
	- `ais_dev` : AIS device (/dev/tty*)
	- `ais_baud` : AIS baudrate (default 38400)

## Generate documentation
Generate documentation in doc/ folder
> cd scripts
> ./gen_doc.sh

## How to run
#### On embedded PC
1. Launch USV control
> roslaunch usv usv_ctrl.launch

#### On ground station
Create virtual serial ports for OpenCPN
> cd scripts
> sudo ./create_ports.bash

Opencpn serial read/write
> roslaunch usv driver_opencpn.launch

## OpenCPN configuration
1. Install and launch `openCPN`
> sudo apt-get install software-properties-common
> sudo add-apt-repository ppa:opencpn/opencpn
> sudo apt-get update
> sudo apt-get install opencpn
1. Use default configuration in `openCPN/default_opencpn.conf` (Need to be copied in `~/.opencpn`). Or :

	### Custom configuration
	1. Connections configuration
		1. Serial In/Out, baud 115200, `/dev/ttyVUSB0`
		Transmit sentences : `RMB`, `WPL`, `RTE`, `APB`
		1. Serial Input, baud 115200, `/dev/ttyVUSB3`
		Ignore sentences : `RMB`, `WPL`, `RTE`, `APB`
	1. Navigation configuration
		Configuration > Ships > Routes/Points : Specify Waypoint Arrival Circle Radius.	
	1. Useful plugins
		- Watchdog : To define No Go Zone and alarm in OpenCPN (Need OCPN Draw).
		- OCPN Draw : To draw areas.
		- oeSENC : To download OpenCPN maps.
		- StatusBar : To display USV info on OpenCPN
		- Dashboard : To display window with USV info (bearing, position, etc...)